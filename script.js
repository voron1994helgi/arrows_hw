// Task 1
const arr = ["travel", "hello", "eat", "ski", "lift"];
let result = arr.filter((item) => {
    if (item.length > 3) {
        return item;
    }
})
console.log(result.length);
// Task 2 
let arrUsers = [
    {
        name: "Іван",
        age: 25,
        sex: "чоловіча",
    },
    {
        name: "Іванна",
        age: 25,
        sex: "жіноча",
    },
    {
        name: "Макар",
        age: 25,
        sex: "чоловіча",
    },
    {
        name: "Олена",
        age: 25,
        sex: "жіноча",
    }
]

let resultUsers = arrUsers.filter((item) => {
    if (item.sex === "чоловіча") {
        return item;
    }
})

console.log(resultUsers);
// Task 3

let arr2 = ['hello', 'world', 23, '23', null];
function filterBy(arr, type) {
    return arr.filter(item => {
        return typeof item !== type;
    });
}
let filteredArray = filterBy(arr2, 'string');
console.log(filteredArray); 